package com.example.room_db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Entities(tableName: String = "db_first_table") {
    @PrimaryKey(autoGenerate = true)
    var ent_id : Int = 0
    @ColumnInfo (name = "Name")
    var ent_name : String = ""
    @ColumnInfo (name = "Designation")
    var ent_desgnation : String = ""
}