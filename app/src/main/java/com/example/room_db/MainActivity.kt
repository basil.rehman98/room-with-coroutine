package com.example.room_db

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Delete
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button = findViewById(R.id.button) as Button
        var db = Room.databaseBuilder(applicationContext, Appdb::class.java, "Mydb")
            .allowMainThreadQueries()
            .build()

        runBlocking {
            var entry = Entities()
            //db.dao().erase(entry)
            //Log.e("check_delete","deleted")
            entry.ent_name = "FAROOQ"
            entry.ent_desgnation = "Sales.Manager"
            launch { db.dao().inserting(entry)
            delay(2000)
            }

            //db.dao().renew(entry)
            //Log.e("check_update","updated")
            async {
                db.dao().readdb().forEach {
                    Log.e("preview", "ID is : ${it.ent_id}")
                    Log.e("preview", "Name is :${it.ent_name}")
                    Log.e("preview", "Designation is : ${it.ent_desgnation}")

                }
            }
                async {
                    button.setOnClickListener {
                        db.dao().deleteById(4)
                    }
                }
            }
        }
    }

