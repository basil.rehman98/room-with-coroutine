package com.example.room_db

import androidx.room.*
import androidx.room.Dao
import java.util.jar.Attributes

@Dao
interface Dao {
    @Insert
    fun inserting(entity: Entities)
//
//    @Query("SELECT * FROM Entities WHERE ent_id IN (:mydb)")
  //  fun loadAllByIds(ent_Id: IntArray): List<Entities>

//    @Query("SELECT * FROM Entities WHERE Name LIKE :name AND " +
//            "Designation LIKE :designation LIMIT 1")
//    fun findByName(Name: String): Entities

    //@Query("Delete FROM Entities WHERE Name :Arsalan")
    //fun erase2(string: Attributes.Name)
    @Query("DELETE FROM Entities WHERE ent_id = :id")
    fun deleteById(id : Int)
    @Query("DELETE FROM Entities WHERE Name = :Arsalan")
    fun deletebyName(Arsalan : String)
    @Query("UPDATE Entities SET Name ='CHAMAN' WHERE ent_id = :id ")
    fun update_record(id: Int)
    @Delete
    fun erase(entity : Entities)
    @Query ("select * from Entities")
    fun readdb() : List<Entities>
    @Update
    fun renew(entity: Entities)
}