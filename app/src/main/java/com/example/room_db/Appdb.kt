package com.example.room_db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database (entities = [(Entities :: class)],version=1)
abstract class Appdb : RoomDatabase() {
    abstract fun dao() : Dao
}